import random
import copy

from types import SimpleNamespace

class Chunks():
	def __init__(self):
		self.chunks = [[]]

	def y(self, y):
		return self.chunks[y]

	def add(self, y, gmap):
		if y > len(self.chunks) - 1:
			self.chunks.append([])
		self.chunks[y].append(gmap)

	def get(self, position=None, x=None, y=None):
		print("position: {}, x: {}, y: {}".format(position, x, y))
		return self.chunks[position.y][position.x] if x is None or y is None else self.chunks[y][x]

	def len(self, y):
		if y > len(self.chunks) - 1:
			return 0
		return len(self.chunks[y])

class Game():
	def __init__(self, seed, x, y):
		self.seed = seed
		self.rng = random.Random(self.seed)
		self.MAX_X = x
		self.MAX_Y = y
		self.map = [[]]
		self.chunks = Chunks()
		self.chunk_position = SimpleNamespace(x=0, y=0)

		self.sprites = {0: '#', 1: '$', 2: '%'}
		self.sprite_codes = {'EARTH': 0, 'RIVER': 1, 'BUSH': 2}

		self.chances_matrix = {
			'EARTH': {'EARTH': 0.5, 'RIVER': 0.3, 'BUSH': 0.2},
			'RIVER': {'EARTH': 0.1, 'RIVER': 0.6, 'BUSH': 0.0},
			'BUSH': {'EARTH': 0.7, 'RIVER': 0.0, 'BUSH': 0.3}
		}

		self.generate()

		self.chunks.add(0, copy.deepcopy(self.map))

	def generate(self, prev_chunk=None):
		self.map = [[]]
		self.generate_earth(prev_chunk)
		self.generate_rivers()

	def generate_earth(self, prev_chunk=None):
		if prev_chunk is None:
			for y in range(self.MAX_Y):
				for x in range(self.MAX_X):
					self.map[y].append(self.sprites[self.sprite_codes['EARTH']])
				self.map.append([])
		else:
			for y in range(self.MAX_Y):
				print(prev_chunk[y][self.MAX_X-1])
				self.map[y].append(prev_chunk[y][self.MAX_X-1])
				self.map.append([])

			for y in range(self.MAX_Y):
				for x in range(1, self.MAX_X):
					self.map[y].append(self.sprites[self.sprite_codes['EARTH']])
		self.print()


	def _get_sprite(self, code):
		return self.sprites[self.sprite_codes[code]]

	#todo: refactor names
	def get_sprite(self, position):
		# print('finding sprite with position {}'.format(position))
		return self.get_sprite_code(self.get_sprite_index(self.map[position.y][position.x]))

	def generate_rivers(self):
		current_river_c = 0
		for y in range(self.MAX_Y):
			for x in range(self.MAX_X):
				index = self.get_sprite_index(self.map[y][x if x == 0 else x-1])
				sprite = self.get_sprite_code(index)
				# print('sprite: %s, index: %d' % (sprite, index))
				chance = self.random()
				for s in self.chances_matrix[sprite]:
					if chance <= self.chances_matrix[sprite][s]:
						# print('current sprite: {}, chance of block: {}, current chance: {}, setting block[{}][{}] {}'.format(sprite, self.chances_matrix[sprite][s], round(chance, 1), x, y, s))
						self.map[y][x] = self._get_sprite(s)

	def get_sprite_index(self, sprite):
		for s in self.sprites:
			if self.sprites[s] == sprite:
				return s

	def get_sprite_code(self, index):
		# print('finding code for index {}'.format(index))
		for c in self.sprite_codes:
			if self.sprite_codes[c] == index:
				return c

	def print(self):
		for y in range(self.MAX_Y):
			for x in range(self.MAX_X):
				print(self.map[y][x], end=' ')
			print('\n')

	def randint(self):
		return self.rng.randint()

	def random(self):
		return self.rng.random()

	def load_map(self, gmap):
		self.map = copy.deepcopy(gmap)

	#todo: Y direction; now returns only X value
	def get_current_chunk(self):
		return self.chunk_position.x

	def move_right(self):
		self.chunk_position.x += 1
		row_len = self.chunks.len(self.chunk_position.y)
		print("len of chunks row: {}, chunk_position: {}".format(row_len, self.chunk_position))
		if self.chunk_position.x < row_len:
			print('loadind chunk')
			self.load_map(self.chunks.get(self.chunk_position))
		else:
			print('generating new chunk')
			self.generate(self.chunks.get(y=self.chunk_position.y, x=self.chunk_position.x-1))
			self.chunks.add(self.chunk_position.y, copy.deepcopy(self.map))

	def move_left(self):
		if self.chunk_position.x > 0:
			self.chunk_position.x -= 1
			self.load_map(self.chunks.get(self.chunk_position))
		print("chunk_position: " + str(self.chunk_position))
