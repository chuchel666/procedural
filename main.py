import sys
import sdl2.ext

from types import SimpleNamespace

from game import Game

RESOURCES = sdl2.ext.Resources(__file__, "resources")

class Position(object):
	def __init__(self, x=0, y=0):
		super(Position, self).__init__()
		self.x = x
		self.y = y

	def __str__(self):
		return "({},{})".format(self.x, self.y)

class Hero(sdl2.ext.Entity):
	def __init__(self, world, sprite, posx, posy, application):
		self.sprite = sprite
		self.sprite.position = posx, posy
		self.position = Position(0, 0)
		self.application = application

	def move(self, direction):
		if direction == 'left':
			if self.position.x <= 0:
				if self.application.game.get_current_chunk() == 0:
					return
				prev_sprite_y = self.sprite.y
				prev_y = self.position.y
				self.application.game.move_left()
				self.application.load_map()
				self.sprite.position = (self.application.game.MAX_X-1) * 45, prev_sprite_y
				self.position = Position(self.application.game.MAX_X-1, prev_y)
			else:
				self.sprite.x -= 45
				self.position.x -=1
		if direction == 'right':
			if self.position.x >= self.application.game.MAX_X-1:
				prev_sprite_y = self.sprite.y
				prev_y = self.position.y
				self.application.game.move_right()
				self.application.load_map()
				self.sprite.position = 0, prev_sprite_y
				self.position = Position(0, prev_y)
			else:
				self.sprite.x += 45
				self.position.x +=1
		if direction == 'up':
			self.sprite.y -= 45
			self.position.y -=1
		if direction == 'down':
			self.sprite.y += 45
			self.position.y +=1

class BasicSprite(sdl2.ext.Entity):
	def __init__(self, world, sprite, posx=0, posy=0, code=None, position=None):
		self.sprite = sprite
		self.sprite.position = posx, posy
		self.code = code
		self.position = position

class BasicSpriteFactory():
	def __init__(self, world, game):
		# todo: load from resource file
		self.game = game
		self.world = world
		self.sprites = {'EARTH' : 'earth.bmp', 'RIVER' : 'water.bmp', 'BUSH' : 'grass.bmp'}
		self.sprite_factory = sdl2.ext.SpriteFactory(sdl2.ext.SOFTWARE)

	def create(self, sprite_index, posx, posy, position):
		sprite = self.sprite_factory.from_image(RESOURCES.get_path(self.sprites[self.game.get_sprite_code(sprite_index)]))
		sprite.depth = 1
		return BasicSprite(self.world, sprite, posx, posy, position)

	def create_sprite(self, code):
		# print('create sprite with code: {}'.format(code))
		return self.sprite_factory.from_image(RESOURCES.get_path(self.sprites[code]))

class MapLoader(sdl2.ext.Applicator):
	def __init__(self, game, factory):
		self.componenttypes = (BasicSprite,)
		self.game = game

	def process(self, world, componentsets):
		#todo: make it works
		for sprite in componentsets:
			game_sprite = self.game.get_sprite(sprite.position)
			sprite.sprite = factory.create_sprite(game.get_sprite_code(game.get_sprite(sprite.position)))


class SoftwareRenderer(sdl2.ext.SoftwareSpriteRenderSystem):
	def __init__(self, window, game):
		self.game = game
		super(SoftwareRenderer, self).__init__(window)

	def render(self, components):
		super(SoftwareRenderer, self).render(components)

class Application(object):
	def __init__(self, game):
		super(Application, self).__init__()

		sdl2.ext.init()

		self.game = game
		self.window = sdl2.ext.Window("Procedural", size=(640, 480))
		self.world = sdl2.ext.World()
		self.spriterenderer = SoftwareRenderer(self.window, self.game)
		self.spritefactory = BasicSpriteFactory(self.world, self.game)
		# self.map_loader = MapLoader(self.game, self.spritefactory)
		self.map = self._create_map()

		self.hero = self.create_hero()

		self.world.add_system(self.spriterenderer)
		# self.world.add_system(self.map_loader)

		self.window.show()

		self.run()

	def _create_map(self):
		x_pos = 0
		y_pos = 0

		_map = []

		for y in range(self.game.MAX_Y):
			for x in range(self.game.MAX_X):
				sprite = self.spritefactory.create(self.game.get_sprite_index(self.game.map[y][x]), x_pos, y_pos, Position(x, y))
				_map.append(sprite)
				x_pos = x_pos+45
			x_pos = 0
			y_pos = y_pos+45

		return _map

	def load_map(self):
		# for sprite in self.map:
		# 	game_sprite = self.game.get_sprite(sprite.position)
		# 	sprite.sprite = self.spritefactory.create_sprite(game_sprite)
		self.map = self._create_map()

	def create_hero(self):
		factory = sdl2.ext.SpriteFactory(sdl2.ext.SOFTWARE)
		hero_sprite = factory.from_image(RESOURCES.get_path('hero.bmp'))
		hero_sprite.depth = 2
		hero = Hero(self.world, hero_sprite, 0, 0, self)

		return hero

	def run(self):
		running = True

		while running:
			events = sdl2.ext.get_events()
			for event in events:
				if event.type == sdl2.SDL_QUIT:
					running = False
					break
				if event.type == sdl2.SDL_KEYDOWN:
					if event.key.keysym.sym == sdl2.SDLK_RIGHT:
						self.hero.move('right')
					if event.key.keysym.sym == sdl2.SDLK_LEFT:
						self.hero.move('left')
					if event.key.keysym.sym == sdl2.SDLK_UP:
						self.hero.move('up')
					if event.key.keysym.sym == sdl2.SDLK_DOWN:
						self.hero.move('down')

			self.world.process()



def main():
	SEED = 666
	X = 8
	Y = 8
	if len(sys.argv) == 2:
		SEED = int(sys.argv[1])
	elif len(sys.argv) == 3:
		SEED = int(sys.argv[1])
		X = int(sys.argv[2])
	elif len(sys.argv) >= 4:
		SEED = int(sys.argv[1])
		X = int(sys.argv[2])
		Y = int(sys.argv[3])

	game = Game(SEED, X, Y)
	game.print()

	app = Application(game)

	return 0

if __name__ == '__main__':
	sys.exit(main())

